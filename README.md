This sample Web application shows how to use Java Bean Validation together with Hibernate Validator in order to validate user objects by using a simple HttpServlet class.
User data is firstly submitted by means of an HTML form, and lastly validated by the servlet. If the data is found valid, a succesful JSP page is displayed. 
Otherwise, the user is forwarded back to the HTML form JSP page, and the corresponding violations are displayed there. 
Beyond its naive nature, the app shows in a nutshell the functionality of Java Bean Validation / Hibernate Validator, and how to use them for encapsulating Domain Model 
validation logic inside a service component, which can be reused everywhere.