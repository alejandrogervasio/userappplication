<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Sign Up</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/reset.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/app.css">
</head>
<body>
    <form action="${pageContext.request.contextPath}/processrequest" method="post">
        <fieldset>

            <c:if test="${violations != null}">
                <c:forEach items="${violations}" var="violation">
                    <p>${violation}.</p>
                </c:forEach>
            </c:if>

            <legend>Sign Up Information</legend>
            <p><label for="name">Name </label><input type="text" name="name" id="name" value="${user.name}"></p>
            <p><label for="email">Email </label><input type="text" name="email" id="email" value="${user.email}"></p>
            <p><label for="biography">Biography </label><textarea name="biography" id="biography">${user.biography}</textarea></p>
            <input type="submit" name="signup" value="Sign Up">
        </fieldset>
    </form>
    <a href="${pageContext.request.contextPath}/test">Test Custom Validator</a>
</body>
</html>