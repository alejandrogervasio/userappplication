package com.electricalweb.modules;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

public class BaseEntityValidator<T> implements EntityValidator<T> {

    protected Validator validator;

    public BaseEntityValidator(Validator validator) {
        this.validator = validator;
    }

    public Set<ConstraintViolation<T>> validate(T t) {
        return validator.validate(t);
    }
}