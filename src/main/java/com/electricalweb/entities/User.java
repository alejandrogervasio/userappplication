package com.electricalweb.entities;

import com.electricalweb.modules.ValidEmail;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class User {

    private int id;

    @NotEmpty(message = "Name is mandatory")
    @Size(min = 2, max = 32, message = "Name must be between 2 and 32 characters long")
    private String name;

    @NotEmpty(message = "Email is mandatory")
    @ValidEmail
    private String email;

    @NotEmpty(message = "Biography is mandatory")
    @Size(min = 10, max = 140, message = "Biography must be between 10 and 140 characters long")
    private String biography;

    public User(String name, String email, String biography) {
        this.name = name;
        this.email = email;
        this.biography = biography;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }
}