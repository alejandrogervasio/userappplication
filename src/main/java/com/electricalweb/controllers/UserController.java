package com.electricalweb.controllers;

import com.electricalweb.entities.User;
import com.electricalweb.modules.BaseEntityValidator;
import com.electricalweb.modules.EntityValidator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Validation;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet(name = "UserController", urlPatterns = "/processrequest")
public class UserController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = getUserFromRequestParameters(request);
        EntityValidator<User> userValidator = getUserValidator();
        List<String> violations = userValidator.validate(user).stream().map(violation -> violation.getMessage()).collect(Collectors.toList());

        if (!violations.isEmpty()) {
            request.setAttribute("violations", violations);
        }

        request.setAttribute("user", user);
        String url = resolveUrl(violations);
        forwardResponse(url, request, response);
    }

    private User getUserFromRequestParameters(HttpServletRequest request) {
        return new User(
                request.getParameter("name"),
                request.getParameter("email"),
                request.getParameter("biography"));
    }

    private EntityValidator<User> getUserValidator() {
        return new BaseEntityValidator<>(Validation.buildDefaultValidatorFactory().getValidator());
    }

    private String resolveUrl(List<String> violations) {
        return !violations.isEmpty() ?  "/" : "/WEB-INF/views/userinfo.jsp";
    }

    private void forwardResponse(String url, HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}