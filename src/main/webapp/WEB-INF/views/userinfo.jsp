<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Data</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/reset.css">
</head>
<body>
    <h1>User Data</h1>
    <p>First Name : ${user.name}</p>
    <p>Email : ${user.email}</p>
    <p>Biography : ${user.biography}</p>
</body>
</html>